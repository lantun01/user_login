import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Put,
  NotFoundException,
} from '@nestjs/common';
import { TasksService } from './tasks.service';
import { CreateTaskDto } from './dto/create-task.dto';
import { UpdateTaskDto } from './dto/update-task.dto';
import { Task } from './entities/task.entity';
import { EstadoTarea } from '../utility/common/estado.enum';

@Controller('tasks')
export class TasksController {
  constructor(private readonly taskService: TasksService) {}

  @Get()
  async findAll(): Promise<Task[]> {
    try {
      return await this.taskService.findAll();
    } catch (error) {
      console.error('Error al obtener todas las tareas:', error);
      throw new NotFoundException('Error al obtener todas las tareas');
    }
  }

  @Get(':id')
  async findOne(@Param('id') id: number): Promise<Task> {
    try {
      return await this.taskService.findOne(id);
    } catch (error) {
      console.error('Error al obtener la tarea por ID:', error);
      throw new NotFoundException('Error al obtener la tarea por ID');
    }
  }

  @Post()
  async create(@Body() createTaskDto: CreateTaskDto): Promise<Task> {
    try {
      return await this.taskService.create(createTaskDto);
    } catch (error) {
      console.error('Error al crear la tarea:', error);
      throw new NotFoundException('Error al crear la tarea');
    }
  }

  @Put(':id')
  async update(
    @Param('id') id: number,
    @Body() updateTaskDto: UpdateTaskDto,
  ): Promise<Task> {
    try {
      return await this.taskService.update(id, updateTaskDto);
    } catch (error) {
      console.error('Error al actualizar la tarea:', error);
      throw new NotFoundException('Error al actualizar la tarea');
    }
  }

  @Put()
  async updatev2(@Body() updateTaskDto: UpdateTaskDto): Promise<Task> {
    try {
      return await this.taskService.updatev2(updateTaskDto);
    } catch (error) {
      console.error('Error al actualizar la tarea (v2):', error);
      throw new NotFoundException('Error al actualizar la tarea (v2)');
    }
  }
}
