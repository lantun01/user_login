import { Entity, PrimaryGeneratedColumn, Column, ManyToOne } from 'typeorm';
import { User } from 'src/modules/users/entities/user.entity';
import { Project } from 'src/modules/projects/entities/project.entity';
import { EstadoTarea } from 'src/modules/utility/common/estado.enum';
import { IsNotEmptyObject } from 'class-validator';

@Entity()
export class Task {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  description: string;

  @ManyToOne(() => User, user => user.tasksCreated)
  @IsNotEmptyObject()
  creator: User;

  @ManyToOne(() => User, user => user.tasksResponsible)
  @IsNotEmptyObject()
  responsible: User;

  @Column({ type: 'enum', enum: EstadoTarea, default: EstadoTarea.PENDIENTE })
  taskStatus: EstadoTarea;

  @Column()
  startDate: Date;

  @Column()
  endDate: Date;

  @ManyToOne(() => Project, project => project.tasks)
  @IsNotEmptyObject()
  project: Project;

  @Column({ default: true }) 
  visible: boolean;
}
