import { Inject, Injectable, forwardRef } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Task } from './entities/task.entity';
import { CreateTaskDto } from './dto/create-task.dto';
import { UpdateTaskDto } from './dto/update-task.dto';
import { UsersService } from '../users/users.service';
import { Project } from '../projects/entities/project.entity';

@Injectable()
export class TasksService {
  constructor(
    @InjectRepository(Task) private readonly taskRepository: Repository<Task>,
    @Inject(forwardRef(() => UsersService))
    private readonly userService: UsersService,
    @InjectRepository(Project)
    private readonly projectRepository: Repository<Project>,
  ) {}

  async create(createTaskDto: CreateTaskDto): Promise<Task> {
    const task = this.taskRepository.create(createTaskDto);
    const creatoruser = await this.userService.findOne(createTaskDto.creatorID);
    const responsible = await this.userService.findName(
      createTaskDto.responsiblename,
    );

    const project = await this.projectRepository.findOne({
      where: {
        name: createTaskDto.projectname,
      },
    });

    if (!creatoruser || !responsible || !project) {
      throw new Error('creator or responsible or project not found');
    }

    task.creator = creatoruser;

    task.responsible = responsible;

    task.project = project;

    return this.taskRepository.save(task);
  }

  findAll(): Promise<Task[]> {
    return this.taskRepository.find({
      where: { visible: true },
      relations: ['creator', 'responsible', 'project'],
    });
  }

  async findOne(id: number): Promise<Task | undefined> {
    return this.taskRepository.findOne({
      where: { id, visible: true },
      relations: ['creator'],
    });
  }

  async update(id: number, updateTaskDto: UpdateTaskDto): Promise<Task> {
    await this.taskRepository.update(id, updateTaskDto);
    return this.taskRepository.findOne({ where: { id, visible: true } });
  }

  async updatev2(updateTaskDto: UpdateTaskDto): Promise<Task> {
    const numero = updateTaskDto.id;
    await this.taskRepository.update(updateTaskDto.id, updateTaskDto);
    return this.taskRepository.findOne({
      where: { id: numero, visible: true },
    });
  }
}
