import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Put,
  BadRequestException,
  NotFoundException,
} from '@nestjs/common';
import { ProjectsService } from './projects.service';
import { CreateProjectDto } from './dto/create-project.dto';
import { UpdateProjectDto } from './dto/update-project.dto';
import { Project } from './entities/project.entity';

@Controller('projects')
export class ProjectsController {
  constructor(private readonly projectService: ProjectsService) {}

  @Get()
  async findAll(): Promise<Project[]> {
    try {
      return await this.projectService.findAll();
    } catch (error) {
      console.error('Error al obtener todos los proyectos:', error);
      throw new BadRequestException('Error al obtener todos los proyectos');
    }
  }

  @Get(':id')
  async findOne(@Param('id') id: number): Promise<Project> {
    try {
      const project = await this.projectService.findOne(id);
      if (!project) {
        throw new NotFoundException('Proyecto no encontrado');
      }
      return project;
    } catch (error) {
      console.error('Error al obtener el proyecto por ID:', error);
      throw new BadRequestException('Error al obtener el proyecto por ID');
    }
  }

  @Post()
  async create(@Body() createProjectDto: CreateProjectDto): Promise<Project> {
    try {
      return await this.projectService.create(createProjectDto);
    } catch (error) {
      console.error('Error al crear el proyecto:', error);
      throw new BadRequestException('Error al crear el proyecto');
    }
  }

  @Put(':id')
  async update(
    @Param('id') id: number,
    @Body() updateProjectDto: UpdateProjectDto,
  ): Promise<Project> {
    try {
      const updatedProject = await this.projectService.update(
        id,
        updateProjectDto,
      );
      if (!updatedProject) {
        throw new NotFoundException('Proyecto no encontrado');
      }
      return updatedProject;
    } catch (error) {
      console.error('Error al actualizar el proyecto:', error);
      throw new BadRequestException('Error al actualizar el proyecto');
    }
  }

  @Put()
  async updatev2(@Body() updateProjectDto: UpdateProjectDto): Promise<Project> {
    try {
      return await this.projectService.updatev2(updateProjectDto);
    } catch (error) {
      console.error('Error al actualizar el proyecto (v2):', error);
      throw new BadRequestException('Error al actualizar el proyecto (v2)');
    }
  }

  @Delete(':id')
  async remove(@Param('id') id: number): Promise<void> {
    try {
      await this.projectService.remove(id);
    } catch (error) {
      console.error('Error al eliminar el proyecto:', error);
      throw new BadRequestException('Error al eliminar el proyecto');
    }
  }
}
