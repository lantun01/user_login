import { CreateProjectDto } from './dto/create-project.dto';
import { UpdateProjectDto } from './dto/update-project.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Project } from './entities/project.entity';
import { Task } from '../tasks/entities/task.entity';
export class ProjectsService {
  constructor(
    @InjectRepository(Project)
    private readonly projectRepository: Repository<Project>,
  ) {}

  async create(createProjectDto: CreateProjectDto): Promise<Project> {
    const project = this.projectRepository.create(createProjectDto);
    return this.projectRepository.save(project);
  }

  findAll(): Promise<Project[]> {
    return this.projectRepository.find({
      where: { visible: true },
      relations: ['teams', 'tasks'],
    });
  }

  findOne(id: number): Promise<Project> {
    return this.projectRepository.findOne({ where: { id } });
  }
  findName(name: string) {
    return this.projectRepository.findOneBy({ name });
  }
  async update(
    id: number,
    updateProjectDto: UpdateProjectDto,
  ): Promise<Project> {
    await this.projectRepository.update(id, updateProjectDto);
    return this.projectRepository.findOne({ where: { id } });
  }
  async updatev2(updateProjectDto: UpdateProjectDto): Promise<Project> {
    const numero = updateProjectDto.id;
    await this.projectRepository.update(updateProjectDto.id, updateProjectDto);
    return this.projectRepository.findOne({
      where: { id: numero, visible: true },
    });
  }

  async remove(id: number): Promise<void> {
    await this.projectRepository.delete(id);
  }

  async getTasksByProject(id: number): Promise<Task[]> {
    const project = await this.findOne(id);
    return project.tasks;
  }
}
