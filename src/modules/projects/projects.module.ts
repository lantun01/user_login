import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ProjectsService } from './projects.service';
import { ProjectsController } from './projects.controller';
import { Project } from './entities/project.entity';
import { TeamModule } from '../team/team.module';
import { TaskModule } from '../tasks/tasks.module';

@Module({
  imports: [TypeOrmModule.forFeature([Project]), TeamModule, TaskModule],
  controllers: [ProjectsController],
  providers: [ProjectsService],
  exports:[ProjectsService]
})
export class ProjectModule {}