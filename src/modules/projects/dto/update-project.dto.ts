import { PartialType } from '@nestjs/mapped-types';
import { IsString, IsOptional, IsBoolean, IsNumber } from 'class-validator';
import { CreateProjectDto } from './create-project.dto';

export class UpdateProjectDto extends PartialType(CreateProjectDto){
    @IsOptional()
    @IsString()
    name?: string;
  
    @IsOptional()
    @IsString()
    description?: string;

    @IsBoolean()
    @IsOptional()
    visible?: boolean;

    @IsNumber()
    @IsOptional()
    id?: number;
  }