import { Entity, PrimaryGeneratedColumn, Column, ManyToMany, JoinTable, OneToMany, Timestamp, CreateDateColumn } from 'typeorm';
import { TeamEntity } from 'src/modules/team/entities/team.entity';
import { Task } from 'src/modules/tasks/entities/task.entity';
import { IsNotEmptyObject } from 'class-validator';
@Entity()
export class Project {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  description: string;

  @CreateDateColumn()
  startDate:Timestamp;

  @ManyToMany(() => TeamEntity, team => team.projects)
  @JoinTable()
  @IsNotEmptyObject()
  teams: TeamEntity[];

  @OneToMany(() => Task, task => task.project)
  @IsNotEmptyObject()
  tasks: Task[];

  @Column({ default: true }) 
  visible: boolean;
}