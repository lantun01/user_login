import {
  UnauthorizedException,
  Injectable,
  BadRequestException,
} from '@nestjs/common';
import { UsersService } from '../users/users.service';
import { RegisterDto } from './dto/register.dto';
import { hash, compare } from 'bcrypt';
import { LoginDto } from './dto/login.dto';
import { User } from '../users/entities/user.entity';
import { sign } from 'jsonwebtoken';
import { ResetDto } from './dto/reset.dto';
import { MailerService } from '@nestjs-modules/mailer';
import * as crypto from 'crypto';
import { ChangePasswordDto } from './dto/changePassword.dto';

@Injectable()
export class AuthService {
  constructor(
    private readonly userService: UsersService,
    private readonly mailerservice: MailerService,
  ) {}

  async register(registerDto: RegisterDto) {
    const user = await this.userService.findOneByEmail(registerDto.email);

    if (user) throw new BadRequestException('user alredy exists');

    registerDto.password = await hash(registerDto.password, 10); //esto hashea la contrasena

    const user2 = await this.userService.create(registerDto); //guarda el usuario
    const accesstoken = await this.accessToken(user2);

    return { accesstoken, user };
  }

  async login(loginDto: LoginDto) {
    const user = await this.userService.findOneByEmail(loginDto.email);

    if (!user) throw new UnauthorizedException('email incorrecto');

    const isPasswordValid = await compare(loginDto.password, user.password);

    if (!isPasswordValid)
      throw new UnauthorizedException('contrasena incorrecta');

    const accesstoken = await this.accessToken(user);

    delete user.password;

    return { accesstoken, user };
  }

  async accessToken(user: User): Promise<string> {
    return sign(
      { id: user.id, email: user.email },
      process.env.ACCESS_TOKEN_SECRET_KEY,
      { expiresIn: process.env.ACCESS_TOKEN_EXPIRE_TIME },
    );
  }

  async generateResetCode(): Promise<string> {
    return new Promise((resolve, reject) => {
      crypto.randomBytes(4, (err, buf) => {
        if (err) {
          reject(err);
        } else {
          resolve(buf.toString('hex').toUpperCase());
        }
      });
    });
  }

  async requestResetPassword(reset: ResetDto): Promise<void> {
    const { email } = reset;
    const user: User = await this.userService.findOneByEmail(email);

    if (user) {
      const resetCode = await this.generateResetCode();
      this.userService.update(user.id, { resetPasswordToken: resetCode });
      await this.sendResetCodeByEmail(user.email, resetCode);
    }
  }

  async sendResetCodeByEmail(email: string, resetCode: string): Promise<void> {
    await this.mailerservice.sendMail({
      to: email,
      from: process.env.USER_MAIL,
      subject: 'Código de Restablecimiento de Contraseña',
      text: `Tu código de restablecimiento de contraseña es: ${resetCode}`,
      html: `<p>Tu código de restablecimiento de contraseña es: <strong>${resetCode}</strong></p>`,
    });
  }

  async comparePassword(email: string, resetCode: string): Promise<boolean> {
    const user = await this.userService.findOneByEmail(email);

    if (!user || !user.resetPasswordToken) {
      console.error('Usuario o código de restablecimiento no válido');
      throw new BadRequestException(
        'Usuario o código de restablecimiento no válido',
      );
    }

    const isPasswordValid = resetCode === user.resetPasswordToken;

    return isPasswordValid;
  }

  async changePassword(dto: ChangePasswordDto): Promise<{ success: boolean }> {
    try {
      const { email, newPassword } = dto;

      const user = await this.userService.findOneByEmail(email);

      if (!user) {
        throw new UnauthorizedException('Usuario no encontrado');
      }

      const hashedPassword = await hash(newPassword, 10);

      user.password = hashedPassword;
      user.resetPasswordToken = null;

      await this.userService.save(user);

      return { success: true };
    } catch (error) {
      console.error('Error changing password:', error);

      if (
        error instanceof UnauthorizedException ||
        error instanceof BadRequestException
      ) {
        throw error;
      }

      throw new BadRequestException(
        'Hubo un error al cambiar la contraseña. Verifica los detalles e intenta nuevamente.',
      );
    }
  }
}
