import {
  Body,
  Controller,
  Patch,
  Post,
  Get,
  BadRequestException,
} from '@nestjs/common';
import { AuthService } from './auth.service';
import { RegisterDto } from './dto/register.dto';
import { LoginDto } from './dto/login.dto';
import { ResetDto } from './dto/reset.dto';
import { ChangePasswordDto } from './dto/changePassword.dto';

@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  async register(@Body() registerDto: RegisterDto) {
    try {
      const result = await this.authService.register(registerDto);
      return result;
    } catch (error) {
      console.error('Error during registration:', error);
      throw error;
    }
  }

  @Post('login')
  async login(@Body() loginDto: LoginDto) {
    try {
      const result = await this.authService.login(loginDto);
      return result;
    } catch (error) {
      console.error('Error during login:', error);
      throw error;
    }
  }

  @Patch('reset')
  async resetPassword(@Body() reset: ResetDto): Promise<void> {
    try {
      await this.authService.requestResetPassword(reset);
    } catch (error) {
      console.error('Error during password reset request:', error);
      throw error;
    }
  }

  @Post('compare-code')
  async comparePassword(
    @Body() body: { email: string; resetCode: string },
  ): Promise<{ success: boolean; error?: string }> {
    try {
      const { email, resetCode } = body;
      const isPasswordValid = await this.authService.comparePassword(
        email,
        resetCode,
      );

      return { success: isPasswordValid };
    } catch (error) {
      console.error('Error comparando código:', error);
      return { success: false, error: 'Hubo un error al comparar el código.' };
    }
  }

  @Post('change-password')
  async changePassword(@Body() dto: ChangePasswordDto) {
    try {
      const result = await this.authService.changePassword(dto);
      return result;
    } catch (error) {
      console.error('Error changing password:', error);
      throw new BadRequestException(
        'Hubo un error al cambiar la contraseña. Verifica los detalles e intenta nuevamente.',
      );
    }
  }
}
