import { Module, forwardRef } from '@nestjs/common';
import { UsersService } from './users.service';
import { UsersController } from './users.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from './entities/user.entity';
import { TaskModule } from '../tasks/tasks.module';

@Module({
  imports :[TypeOrmModule.forFeature([User]),forwardRef(() => TaskModule)],
  controllers: [UsersController],
  providers: [UsersService],
  exports: [UsersService] //exporto user service para que pueda ser usado por otro modulo como auth,pero auth debe recibirlo tambien
})
export class UsersModule {}
