import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Put,
  BadRequestException,
  NotFoundException,
} from '@nestjs/common';
import { UsersService } from './users.service';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { User } from './entities/user.entity';

@Controller('users')
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  @Post()
  async create(@Body() createUserDto: CreateUserDto) {
    try {
      const createdUser = await this.usersService.create(createUserDto);
      return createdUser;
    } catch (error) {
      if (
        error instanceof Error &&
        error.message === 'Alguna condición específica'
      ) {
        throw new BadRequestException('Mensaje de error personalizado');
      }

      throw new BadRequestException('No se pudo crear el usuario');
    }
  }

  @Get()
  async findAll() {
    try {
      const users = await this.usersService.findAll();
      return users;
    } catch (error) {
      throw new NotFoundException('Users not found');
    }
  }

  @Get(':id')
  async findOne(@Param('id') id: string) {
    try {
      const user = await this.usersService.findOne(+id);
      if (!user) {
        throw new NotFoundException('User not found');
      }
      return user;
    } catch (error) {
      throw new BadRequestException('Invalid user ID');
    }
  }

  @Put(':id')
  async update(@Param('id') id: number, @Body() updateUserDto): Promise<User> {
    try {
      const updatedUser = await this.usersService.update(id, updateUserDto);
      return updatedUser;
    } catch (error) {
      throw new NotFoundException('User not found');
    }
  }

  @Put()
  async delete(@Body() updateUserDto: UpdateUserDto): Promise<User> {
    try {
      const deletedUser = await this.usersService.delete(updateUserDto);
      return deletedUser;
    } catch (error) {
      throw new NotFoundException('User not found');
    }
  }

  @Put(':userId/assign-task/:taskId')
  async assignTaskToUser(
    @Param('userId') userId: number,
    @Param('taskId') taskId: number,
  ): Promise<void> {
    try {
      await this.usersService.assignTaskToUser(userId, taskId);
    } catch (error) {
      throw new NotFoundException('User or task not found');
    }
  }
}
