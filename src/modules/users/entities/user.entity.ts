import { Task } from 'src/modules/tasks/entities/task.entity';
import { TeamEntity } from 'src/modules/team/entities/team.entity';
import {
  Column,
  Entity,
  JoinTable,
  ManyToMany,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Roles } from 'src/modules/utility/common/user-roles.enum';
@Entity({ name: 'users' })
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ nullable: false })
  name: string;

  @Column({ unique: true, nullable: false })
  email: string;

  @Column({ nullable: false })
  password: string;

  @ManyToMany(() => TeamEntity, (team) => team.users)
  @JoinTable()
  teams: TeamEntity[];

  @OneToMany(() => Task, (task) => task.creator)
  tasksCreated: Task[];

  @OneToMany(() => Task, (task) => task.responsible)
  tasksResponsible: Task[];

  @Column({
    type: 'simple-enum',
    enum: Roles,
    array: true,
    default: [Roles.USER],
  })
  roles: Roles[];

  @Column({ name: 'resetPasswordToken', nullable: true })
  resetPasswordToken: string;

  @Column({ default: true })
  visible: boolean;
}
