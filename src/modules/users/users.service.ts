import { Inject, Injectable, NotFoundException, forwardRef } from '@nestjs/common';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from './entities/user.entity';
import { Repository } from 'typeorm';
import { TasksService } from '../tasks/tasks.service';

@Injectable()
export class UsersService {

  constructor(
    @InjectRepository(User) private readonly userRepository: Repository<User>,
    @Inject(forwardRef(() => TasksService)) private readonly taskService: TasksService,
  ) {}

  async create(createUserDto: CreateUserDto): Promise<User> {
    const newUser = this.userRepository.create(createUserDto);
    return this.userRepository.save(newUser);
  }

  async findOneByEmail(email: string): Promise<User | undefined> {
    return this.userRepository.findOne({ where: { email } });
  }

  async findName(name: string): Promise<User> {
    const user = await this.userRepository.findOne({ where: { name } });
    if (!user) throw new NotFoundException('User not found.');
    return user;
  }

  async findAll(): Promise<User[]> {
    return this.userRepository.find();
  }

  async findOne(id: number): Promise<User> {
    const user = await this.userRepository.findOne({
      where: { id },
      relations: { teams: true },
    });
    if (!user) throw new NotFoundException('User not found.');
    return user;
  }

  async update(id: number, updateUserDto: UpdateUserDto): Promise<User> {
    await this.userRepository.update(id, updateUserDto);
    return this.userRepository.findOne({ where: { id, visible: true } });
  }

  async delete(updateUserDto: UpdateUserDto): Promise<User> {
    const userId = updateUserDto.id;
    await this.userRepository.update(userId, updateUserDto);
    return this.userRepository.findOne({ where: { id: userId, visible: true } });
  }

  async assignTaskToUser(userId: number, taskId: number): Promise<void> {
    const user = await this.userRepository.findOne({ where: { id: userId } });
    const task = await this.taskService.findOne(taskId);

    if (!user || !task) {
      throw new Error('User or Task not found');
    }

    user.tasksCreated = [...user.tasksCreated, task];
    await this.userRepository.save(user);
  }

  async save(user: User): Promise<User> {
    return this.userRepository.save(user);
  }
}