export enum EstadoTarea {
    PENDIENTE = 'pendiente',
    EN_PROGRESO = 'en progreso',
    TERMINADA = 'terminada',
  }