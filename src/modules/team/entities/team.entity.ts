import { User } from 'src/modules/users/entities/user.entity';
import {
  Column,
  Entity,
  JoinTable,
  ManyToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Project } from 'src/modules/projects/entities/project.entity';
import { Roles } from 'src/modules/utility/common/user-roles.enum';
@Entity('team')
export class TeamEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @ManyToMany(() => User, (user) => user.teams)
  users: User[];

  @ManyToMany(() => Project, (project) => project.teams)
  @JoinTable()
  projects: Project[];

  @Column({
    type: 'simple-enum',
    enum: Roles,
    array: true,
    default: [Roles.USER],
  })
  userRoles: Roles[];

  @Column({ default: true })
  visible: boolean;
}
