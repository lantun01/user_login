import { IsNumber } from 'class-validator';

export class AgregarUsers {
  @IsNumber()
  id_user: number;
  @IsNumber()
  id_team: number;
}
