import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  NotFoundException,
} from '@nestjs/common';
import { TeamService } from './team.service';
import { CreateTeamDto } from './dto/create-team.dto';
import { UpdateTeamDto } from './dto/update-team.dto';
import { TeamEntity } from './entities/team.entity';
import { User } from '../users/entities/user.entity';
import { AgregarUsers } from './dto/agregar_user.dto';

@Controller('team')
export class TeamController {
  constructor(private readonly teamService: TeamService) {}

  @Post()
  async create(@Body() createTeamDto: CreateTeamDto): Promise<TeamEntity> {
    try {
      return await this.teamService.create(createTeamDto);
    } catch (error) {
      console.error('Error al crear el equipo:', error);
      throw new NotFoundException('Error al crear el equipo');
    }
  }

  @Get()
  async findAll(): Promise<TeamEntity[]> {
    try {
      return await this.teamService.findAll();
    } catch (error) {
      console.error('Error al obtener todos los equipos:', error);
      throw new NotFoundException('Error al obtener todos los equipos');
    }
  }

  @Get(':id')
  async findOne(@Param('id') id: string): Promise<TeamEntity> {
    try {
      return await this.teamService.findOne(+id);
    } catch (error) {
      console.error('Error al obtener el equipo por ID:', error);
      throw new NotFoundException('Error al obtener el equipo por ID');
    }
  }

  @Get('users/:id')
  async findUsers(@Param('id') id: string): Promise<User[]> {
    try {
      return await this.teamService.findUsers(+id);
    } catch (error) {
      console.error('Error al obtener los usuarios del equipo por ID:', error);
      throw new NotFoundException(
        'Error al obtener los usuarios del equipo por ID',
      );
    }
  }

  @Patch(':id')
  async update(
    @Param('id') id: string,
    @Body() updateTeamDto: UpdateTeamDto,
  ): Promise<TeamEntity> {
    try {
      return await this.teamService.update(+id, updateTeamDto);
    } catch (error) {
      console.error('Error al actualizar el equipo:', error);
      throw new NotFoundException('Error al actualizar el equipo');
    }
  }

  @Post('/party')
  async updateParty(@Body() agregar: AgregarUsers): Promise<TeamEntity> {
    try {
      return await this.teamService.updateParty(agregar);
    } catch (error) {
      console.error('Error al actualizar la fiesta del equipo:', error);
      throw new NotFoundException('Error al actualizar la fiesta del equipo');
    }
  }

  @Delete(':id')
  async remove(@Param('id') id: string): Promise<void> {
    try {
      await this.teamService.remove(+id);
    } catch (error) {
      console.error('Error al eliminar el equipo:', error);
      throw new NotFoundException('Error al eliminar el equipo');
    }
  }
}
