import {
  Injectable,
  NotFoundException,
  BadRequestException,
} from '@nestjs/common';
import { CreateTeamDto } from './dto/create-team.dto';
import { UpdateTeamDto } from './dto/update-team.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { TeamEntity } from './entities/team.entity';
import { Repository } from 'typeorm';
import { UsersService } from '../users/users.service';
import { User } from '../users/entities/user.entity';
import { AgregarUsers } from './dto/agregar_user.dto';

@Injectable()
export class TeamService {
  constructor(
    @InjectRepository(TeamEntity)
    private teamRepository: Repository<TeamEntity>,
    private readonly userService: UsersService,
  ) {}

  async create(createTeamDto: CreateTeamDto) {
    return await this.teamRepository.save(createTeamDto);
  }

  async findAll() {
    return await this.teamRepository.find();
  }

  async findOne(id: number): Promise<TeamEntity> {
    const team = await this.teamRepository.findOne({
      where: { id: id },
      relations: { users: true },
    });
    if (!team) throw new NotFoundException('team not found.');
    return team;
  }

  async update(id: number, updateTeamDto: UpdateTeamDto) {
    const team = await this.findOne(id);

    if (!team) throw new NotFoundException('team no encontrada');

    Object.assign(team, updateTeamDto);
    return await this.teamRepository.save(team);
  }

  async updateParty(agregar: AgregarUsers) {
    const team = await this.findOne(agregar.id_team);

    if (!team) throw new NotFoundException('team no encontrada');

    const user = await this.userService.findOne(agregar.id_user);

    if (!user) throw new NotFoundException('user no encontrada');

    if (team.users.includes(user))
      throw new BadRequestException('este elemento esta en la lista');
    team.users.push(user);

    return await this.teamRepository.save(team);
  }

  async remove(id: number) {
    const team = await this.findOne(id);

    if (!team) throw new NotFoundException('team no encontrada');

    return await this.teamRepository.remove(team);
  }

  async findUsers(id: number): Promise<User[]> {
    const team = await this.teamRepository.findOne({
      where: { id: id },
      relations: { users: true },
    });
    if (!team) throw new NotFoundException('team not found.');
    return team.users;
  }
}
