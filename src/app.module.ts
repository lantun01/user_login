import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UsersModule } from './modules/users/users.module';
import { AuthModule } from './modules/auth/auth.module';
import { TeamModule } from './modules/team/team.module';
import { dataSourceOptions } from 'db/data-source';
import { TaskModule } from './modules/tasks/tasks.module';
import { ProjectModule } from './modules/projects/projects.module';
import { MailerModule } from '@nestjs-modules/mailer';


@Module({
  imports: [TypeOrmModule.forRoot(dataSourceOptions),
    UsersModule,
    AuthModule,
    TeamModule,
    TaskModule,
    ProjectModule,
    MailerModule.forRoot({
      transport:{
        host: 'smtp.gmail.com',
        port: 587,
        secure: false,
        auth:{
          user:process.env.USER_MAIL,
          pass:process.env.USER_PASSWORD
        }
      }
    })
  ],
  providers: []
})
export class AppModule {}
