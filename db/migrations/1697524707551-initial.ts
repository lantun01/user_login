import { MigrationInterface, QueryRunner } from "typeorm";

export class Initial1697524707551 implements MigrationInterface {
    name = 'Initial1697524707551'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "users" ("id" SERIAL NOT NULL, "name" character varying NOT NULL, "email" character varying NOT NULL, "password" character varying NOT NULL, CONSTRAINT "UQ_97672ac88f789774dd47f7c8be3" UNIQUE ("email"), CONSTRAINT "PK_a3ffb1c0c8416b9fc6f907b7433" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "tema" ("id" SERIAL NOT NULL, "name" character varying NOT NULL, CONSTRAINT "PK_737d3af5e90b2418bd0b2faa309" PRIMARY KEY ("id"))`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP TABLE "tema"`);
        await queryRunner.query(`DROP TABLE "users"`);
    }

}
