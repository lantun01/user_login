import { MigrationInterface, QueryRunner } from "typeorm";

export class ProyectTask1700823413514 implements MigrationInterface {
    name = 'ProyectTask1700823413514'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TYPE "public"."task_taskstatus_enum" AS ENUM('pendiente', 'en progreso', 'terminada')`);
        await queryRunner.query(`CREATE TABLE "task" ("id" SERIAL NOT NULL, "name" character varying NOT NULL, "description" character varying NOT NULL, "taskStatus" "public"."task_taskstatus_enum" NOT NULL DEFAULT 'pendiente', "startDate" TIMESTAMP NOT NULL, "endDate" TIMESTAMP NOT NULL, "creatorId" integer, "responsibleId" integer, "projectId" integer, CONSTRAINT "PK_fb213f79ee45060ba925ecd576e" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "project" ("id" SERIAL NOT NULL, "name" character varying NOT NULL, "description" character varying NOT NULL, "startDate" TIMESTAMP NOT NULL, CONSTRAINT "PK_4d68b1358bb5b766d3e78f32f57" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "team_projects_project" ("teamId" integer NOT NULL, "projectId" integer NOT NULL, CONSTRAINT "PK_02cefcae57745b0c241b712ea7d" PRIMARY KEY ("teamId", "projectId"))`);
        await queryRunner.query(`CREATE INDEX "IDX_91997efd025c2003a88699c74a" ON "team_projects_project" ("teamId") `);
        await queryRunner.query(`CREATE INDEX "IDX_cccb2c8892944892a835ec0246" ON "team_projects_project" ("projectId") `);
        await queryRunner.query(`CREATE TABLE "project_teams_team" ("projectId" integer NOT NULL, "teamId" integer NOT NULL, CONSTRAINT "PK_cd2b83dd2c39ca5da0ae0d66ab1" PRIMARY KEY ("projectId", "teamId"))`);
        await queryRunner.query(`CREATE INDEX "IDX_a7d311f774c34b41ec18ed14f5" ON "project_teams_team" ("projectId") `);
        await queryRunner.query(`CREATE INDEX "IDX_6f617c3f27c84b38c8e35837ff" ON "project_teams_team" ("teamId") `);
        await queryRunner.query(`CREATE TYPE "public"."team_userroles_enum" AS ENUM('dev', 'productor', 'SCRUM', 'PD', 'ux', 'user')`);
        await queryRunner.query(`ALTER TABLE "team" ADD "userRoles" "public"."team_userroles_enum" array NOT NULL DEFAULT '{user}'`);
        await queryRunner.query(`ALTER TABLE "task" ADD CONSTRAINT "FK_94fe6b3a5aec5f85427df4f8cd7" FOREIGN KEY ("creatorId") REFERENCES "users"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "task" ADD CONSTRAINT "FK_4ba68e3e65410dfd632913e4373" FOREIGN KEY ("responsibleId") REFERENCES "users"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "task" ADD CONSTRAINT "FK_3797a20ef5553ae87af126bc2fe" FOREIGN KEY ("projectId") REFERENCES "project"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "team_projects_project" ADD CONSTRAINT "FK_91997efd025c2003a88699c74ae" FOREIGN KEY ("teamId") REFERENCES "team"("id") ON DELETE CASCADE ON UPDATE CASCADE`);
        await queryRunner.query(`ALTER TABLE "team_projects_project" ADD CONSTRAINT "FK_cccb2c8892944892a835ec02466" FOREIGN KEY ("projectId") REFERENCES "project"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "project_teams_team" ADD CONSTRAINT "FK_a7d311f774c34b41ec18ed14f52" FOREIGN KEY ("projectId") REFERENCES "project"("id") ON DELETE CASCADE ON UPDATE CASCADE`);
        await queryRunner.query(`ALTER TABLE "project_teams_team" ADD CONSTRAINT "FK_6f617c3f27c84b38c8e35837ff7" FOREIGN KEY ("teamId") REFERENCES "team"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "project_teams_team" DROP CONSTRAINT "FK_6f617c3f27c84b38c8e35837ff7"`);
        await queryRunner.query(`ALTER TABLE "project_teams_team" DROP CONSTRAINT "FK_a7d311f774c34b41ec18ed14f52"`);
        await queryRunner.query(`ALTER TABLE "team_projects_project" DROP CONSTRAINT "FK_cccb2c8892944892a835ec02466"`);
        await queryRunner.query(`ALTER TABLE "team_projects_project" DROP CONSTRAINT "FK_91997efd025c2003a88699c74ae"`);
        await queryRunner.query(`ALTER TABLE "task" DROP CONSTRAINT "FK_3797a20ef5553ae87af126bc2fe"`);
        await queryRunner.query(`ALTER TABLE "task" DROP CONSTRAINT "FK_4ba68e3e65410dfd632913e4373"`);
        await queryRunner.query(`ALTER TABLE "task" DROP CONSTRAINT "FK_94fe6b3a5aec5f85427df4f8cd7"`);
        await queryRunner.query(`ALTER TABLE "team" DROP COLUMN "userRoles"`);
        await queryRunner.query(`DROP TYPE "public"."team_userroles_enum"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_6f617c3f27c84b38c8e35837ff"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_a7d311f774c34b41ec18ed14f5"`);
        await queryRunner.query(`DROP TABLE "project_teams_team"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_cccb2c8892944892a835ec0246"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_91997efd025c2003a88699c74a"`);
        await queryRunner.query(`DROP TABLE "team_projects_project"`);
        await queryRunner.query(`DROP TABLE "project"`);
        await queryRunner.query(`DROP TABLE "task"`);
        await queryRunner.query(`DROP TYPE "public"."task_taskstatus_enum"`);
    }

}
