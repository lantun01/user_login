import { MigrationInterface, QueryRunner } from "typeorm";

export class MejorasEnMany1697596937743 implements MigrationInterface {
    name = 'MejorasEnMany1697596937743'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "users_teams_team" ("usersId" integer NOT NULL, "teamId" integer NOT NULL, CONSTRAINT "PK_096f99173e30e1183a05bc4949c" PRIMARY KEY ("usersId", "teamId"))`);
        await queryRunner.query(`CREATE INDEX "IDX_90b9f667db2161053fbaaff0b3" ON "users_teams_team" ("usersId") `);
        await queryRunner.query(`CREATE INDEX "IDX_482bcf983d5a5c92f43420b6d5" ON "users_teams_team" ("teamId") `);
        await queryRunner.query(`ALTER TABLE "users_teams_team" ADD CONSTRAINT "FK_90b9f667db2161053fbaaff0b3f" FOREIGN KEY ("usersId") REFERENCES "users"("id") ON DELETE CASCADE ON UPDATE CASCADE`);
        await queryRunner.query(`ALTER TABLE "users_teams_team" ADD CONSTRAINT "FK_482bcf983d5a5c92f43420b6d5c" FOREIGN KEY ("teamId") REFERENCES "team"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "users_teams_team" DROP CONSTRAINT "FK_482bcf983d5a5c92f43420b6d5c"`);
        await queryRunner.query(`ALTER TABLE "users_teams_team" DROP CONSTRAINT "FK_90b9f667db2161053fbaaff0b3f"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_482bcf983d5a5c92f43420b6d5"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_90b9f667db2161053fbaaff0b3"`);
        await queryRunner.query(`DROP TABLE "users_teams_team"`);
    }

}
