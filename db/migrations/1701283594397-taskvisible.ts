import { MigrationInterface, QueryRunner } from "typeorm";

export class Taskvisible1701283594397 implements MigrationInterface {
    name = 'Taskvisible1701283594397'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "task" ADD "visible" boolean NOT NULL DEFAULT true`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "task" DROP COLUMN "visible"`);
    }

}
